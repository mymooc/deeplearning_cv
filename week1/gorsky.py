import matplotlib.pyplot as plt
import numpy as np

import os
dir_name='plates'
def load_data(dir_name = dir_name):
    img_extension='.png'
    filelist= [file for file in os.listdir('plates') if file.endswith(img_extension)]
    for file in filelist:
         images = []
    for (root, dirs, files) in os.walk(dir_name):
        for filename in files:
            if filename.endswith(img_extension):
                img_4 = plt.imread(os.path.join(root,filename))
                img_1 = img_4.mean(axis=2)
                images.append(img_1)
    return images

plates = load_data('plates')


# The auxiliary function `visualize()` displays the images given as argument.
np.set_printoptions(threshold=np.inf)

def visualize(imgs, format=None):
    plt.figure(figsize=(20, 40))
    for i, img in enumerate(imgs):
        if img.shape[0] == 3:
            img = img.transpose(1,2,0)
        plt_idx = i+1
        plt.subplot(3, 3, plt_idx)
        plt.imshow(img, cmap=format)
    plt.show()

visualize(plates, 'gray')
for plate in plates:
    print(plate.shape)
print(plates[-1])