# Deep Learning in Computer Vision
by National Research University Higher School of Economics



## Week 1

#### Topics
* Digital images, color models
* Image processing goals and tasks
* Image convolution
* Edge Detection

#### Assignment


## Week 2

#### Topics

##### Image classification
* AlexNet,VGG, Inception architectures
* ResNet and beyond
* Fine-grained image recognition
* Detection and classification of facial attributes

##### Content-based image retrieval
* Computing semantic image embeddings using convolutional neural networks
* Employing indexing structures for efficient retrieval of semantic neighbors
* Face verification
* The re-identification problem in computer vision

##### Keypoints regression
* Facial keypoints regression
* CNN for keypoints regression


## Week 3

##### Sliding window detectors
* Object detection problem
* Sliding window
* HOG based detector
* Detector training
* Viola-Jones face detector
* Attentional cascades and neural networks

##### Modern detector architectures
* Recognition based CNN
* R-CNN, Fast R-CNN, Faster R-CNN
* Region-based fully CNN
* Single shot detectors
* Speed vs accuracy trade-off


## Week 4

##### Object tracking
* Optical Flow
* Deep Learning in optical flow estimation
* Visual object tracking
* Multiple object tracking

##### Action tracking
* Action Classification
* Action localization

## Week 5

##### Image segmentation 
* Segmentation  with DNN
* Human pose estimation as image segmentation task

#####  Style transfer and image generation
* Style Transfer
* GAN
* Image transofrmation with DNN
